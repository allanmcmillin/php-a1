<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Load faker fuctionality
use Faker\Factory;
class User {}
class Tmessage {}
class Message {}

Route::get('/', function () {
// creating faker variable to allow faker to be used
    $faker = Factory::create();
// Giving a name and a picture of the the faux "author" of the page
    $user = new User();
    $user->name = $faker->name;
    $user->image = $faker->imageUrl($width=250,$height=120,'people');
// Creating Message and heading for the whats new section along with hero image
    $message = new Message();
    $message->heroimg = $faker->imageUrl($width=900,$height=500, 'nature');
    $message->whatsnewheading =$faker->bs;
    $message->whatsnew = $faker->text(800);

// Creating message, heading and image and header for the project section
    $message->projectimg =  $faker->imageUrl($width=640,$height=480,'food');
    $message->projectheading = $faker->bs;
    $message->project = $faker->text(500);
// Creating messages and image for the events section
    $message->events = $faker->text(400);
    $message->eventsheading = $faker->bs;
    $message->eventimg = $faker->imageUrl($width=500, $height=600, 'technics' );

// Creating messages for the Giving thanks section
    $tmessage1 = new Tmessage();
        $tmessage1->givingthanks = $faker->text(300);
    $tmessage2 = new Tmessage();
        $tmessage2->givingthanks = $faker->text(300);
    $tmessage3 = new Tmessage();
        $tmessage3->givingthanks = $faker->text(300);
    $tmessage4 = new Tmessage();
        $tmessage4->givingthanks = $faker->text(300);

    $data = [
        'user' => $user,
        'tmessages' => [$tmessage1,$tmessage2,$tmessage3,$tmessage4],
        'message' => $message
    ];

    return view('welcome', $data);
});
