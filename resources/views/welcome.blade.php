@extends('layout')
@section('content')
<section class="hero">
    <a href="http://www.onelifefineart.com/faq.html"> <img src="<?php echo $message->heroimg ?>" alt="Fall in Love with Art: Invest in an inspired painting"></a>
</section>
<section class="clearfix messages">
    <h2>Messages</h2>
    <div class="column1">
        <div class="whatsnew">
            <h3>Happy New Year!</h3>
            <h3><?php echo $message->whatsnewheading ?></h3>
            <p>
                <?php echo $message->whatsnew ?>
            </p>
        </div>
    </div>
    <div class="column2">
        <div class="projects">
            <h3>#payitforward:</h3>
            <a href="http://www.onelifefineart.com/sunflowerproject.html">THE SUNFLOWER PROJECT</a>
            <img src="<?php echo $message->projectimg?>" alt="picture of Sunflower seed">
            <h4><?php echo $message->projectheading ?></h4>
            <p>
                <?php echo $message->project ?>
            </p>
        </div>

    </div>
    <div class="column3">
        <h3>Events:</h3>
        <img src="<?php echo $message->eventimg ?>" alt="">
        <h4><?php echo $message->eventsheading ?></h4>
        <p>
            <?php echo $message->events ?>
        </p>
    </div>
    <br>
    <div class="giving-thanks">
        <h3>Giving Thanks</h3>
        <ul>
            <?php foreach ($tmessages as $tmessage):?>
            <li>
                <?php echo $tmessage->givingthanks ?>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>

</section>
@endsection
