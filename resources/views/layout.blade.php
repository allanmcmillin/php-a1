<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Font Awsome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Project Sytles -->
    <link rel="stylesheet" href="./css/master.css">

    <title>One Life Fine Art: Art of <?php echo $user->name ?></title>
  </head>
  <body>
    <div class="container">
        <div class="mobile-dropdown">
            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="art-menu dropdown-item" href="http://www.onelifefineart.com/projects.html">Art</a>
            <a class="commissions-menu dropdown-item" href="http://www.onelifefineart.com/faq.html">Commissions</a>
            <a class="blog-menu dropdown-item" href="http://onelifefineart.com/blog/">Blog</a>
            <a class="about-menu dropdown-item" href="http://www.onelifefineart.com/about.html">About</a>
            <a class="dropdown-item" href="http://www.onelifefineart.com/sunflowerproject.html">The Sunflower Project</a>
          </div>
        </div>
        <header>

            <img src="<?php echo $user->image?>">
            <div class="header-menu">
                <div class="art-menu"><a class="" href="http://www.onelifefineart.com/projects.html">Art</a></div>
                <div class="commissions-menu"><a href="http://www.onelifefineart.com/faq.html">Commissions</a></div>
                <div class="blog-menu"><a href="http://onelifefineart.com/blog/">Blog</a></div>
                <div class="about-menu"><a href="http://www.onelifefineart.com/about.html">About</a></div>
            </div>
        </header>
        <div class="name-header">
            <img src="http://www.onelifefineart.com/images/OLindex_03.gif" alt="">
            <h1><?php echo $user->name ?></h1>
            <img src="http://www.onelifefineart.com/images/OLindex_03.gif" alt="">
        </div>
        @yield('content')
        <footer>
            <a target="_blank" href="https://www.facebook.com/One-Life-Fine-Art-279190585453584/"><i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i></a>
            <a target="_blank" href="https://twitter.com/d_miszaniec?ref_src=twsrc%5Etfw&ref_url=http%3A%2F%2Fwww.onelifefineart.com%2F"><i class="fa fa-twitter fa-3x" aria-hidden="true"></i></a>
            <a target="_blank" href="https://www.pinterest.ca/d0kevil/"><i class="fa fa-pinterest fa-3x" aria-hidden="true"></i></a>
            <br>
            <a href="http://www.onelifefineart.com/contact.html">Contact</a>
            <h6>All materials contained herein are copyright <?php echo $user->name ?>, Debbie.lee Miszaniec and Lorem Ipsum</h6>
            <h6>Web Site Design by Allan McMillin and Lorem Ipsum</h6>
            <h6>Updated March 3rd 2018</h6>
        </footer>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </div>
</body>
